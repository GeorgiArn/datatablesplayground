@extends('layouts.app')

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#users-table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{route('user-data')}}"
                },
                columns: [
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    }
                ],
                "scrollY": "200px",
                "scrollCollapse": true,
            } );
        } );
    </script>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card m2">
                    <div class="card-header">Users</div>
                    <div class="card-body text-center">
                        <div class="text-md-right mb-4">
                        </div>
                        <table id="users-table" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


