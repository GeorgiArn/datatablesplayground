<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index() {
        return view('user.index');
    }

    public function getUserData() {
        $users = User::select('name', 'email');

        return DataTables::of($users)->make(true);
    }
}
